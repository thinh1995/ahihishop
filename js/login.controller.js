// /**
//  * Created by XuanThinh on 05/06/2016.
//  */
// 'use strict';
//
//Create controler for Login
angular.module('myApp').controller('loginCtrl', ['authFactory', '$scope', '$window', function (authFactory, $scope, $window) {
    $scope.login = function () {
        var result = authFactory.authUser($scope.loginEmail, $scope.loginPassword);
        result.then(function (authData) {
            console.log("User Successfully logged in with uid: ", authData.uid);
            $window.alert("Đăng nhập thành công!!!");
            $window.location.href = "#!/admin";
        }, function(error) {
            console.log("Authentication Failed: ", error);
            $window.alert("Đăng nhập thất bại!!!\n\n\tTên đăng nhập hoặc mật khẩu không đúng.");
            $window.location.href = "#!/login";
        })
    };

    $scope.singup = function () {
        var result = authFactory.createUser($scope.sinupEmail, $scope.sinupPassword);
        result.then(function (userData) {
            console.log("User Successfully created with uid: ", userData.uid);
            $window.alert("Đăng ký thành công!!!");
            $window.location.href = "#!/";
        }, function(error) {
            console.log("an error occurred ", error);
            $window.alert("Đăng ký thất bại!!!\n\n\tTên đăng nhập hoặc mật khẩu không hợp lệ.");
        })

    };

    $scope.reset = function () {
        var result = authFactory.resetUser($scope.resetEmail);
        result.then(function (authData) {
                console.log("Email đặt lại mật khẩu đã được gửi. Vui lòng check mail!");
                //Export a success message
                $window.alert("Email đặt lại mật khẩu đã được gửi. Vui lòng check mail!");
                $window.location.href = "#!/";
            },  function (error) {
                console.log("Email chưa đăng ký!!!", error);
                //Export a fail message
                $window.alert("Email chưa đăng ký!!!");
                $window.location.href = "#!/resetpass";
            })
    };
    
    $scope.OAuth = function () {
        var auth = authFactory.auth();
        auth.$authWithOAuthPopup('google');
        window.location = "#!/";
    }
}]);