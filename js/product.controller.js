/**
 * Created by XuanThinh on 03/06/2016.
 */
'use strict';

var productControllers = angular.module('productControllers', []);
//--------------------------Phones--------------------------------------------------
//Create factory for phone overview
productControllers.factory('phoneOverview', ['$firebaseArray', function ($firebaseArray) {
    //create a reference to the database where we will store our data
    var ref = new Firebase("https://ahihiweb.firebaseio.com/phones/");

    //return it as a synchronized object
    return $firebaseArray(ref);

}]);

//Create controller for phone overview
productControllers.controller('phoneOverviewCtrl', ['$scope', 'phoneOverview',
function ($scope, phoneOverview) {
    $scope.phoneOver  = phoneOverview;

    angular.forEach($scope.phoneOver, function (phone) {
        phone.price = parseFloat(phone.price);
    });

    //Create function buy when click 'Đặt mua'
    $scope.buy = function (index) {
        var customerProductName = $scope.phoneOver.$getRecord(index).name;
        var customerProductPrice = $scope.phoneOver.$getRecord(index).price;
        sessionStorage.setItem("customerProductName", customerProductName);
        sessionStorage.setItem("customerProductPrice", customerProductPrice);
        window.location = "#!/order";
    }

}]);

//Create factory for phone detail
productControllers.factory('phoneDetail', ['$firebaseObject', '$routeParams', function ($firebaseObject, $routeParams) {
    //create a reference to the database where we will store our data
    var phoneId = $routeParams.phoneId;
    var ref = new  Firebase("https://ahihiweb.firebaseio.com/phones/").child(phoneId);

    //return it as a synchronized object
    return $firebaseObject(ref);

}]);

//Create controller for phone detail
productControllers.controller('phoneDetailCtrl', ['$scope', 'phoneDetail', function ($scope, phoneDetail) {
    $scope.phoneDet = phoneDetail;

    //Create function for saving editing phone
    $scope.saveInfo = function () {
        $scope.phoneDet.$save();
        alert("Lưu thành công");
        window.location = "#!/admin";
    };

    //Create function buy when click 'Đặt mua'
    $scope.buy = function () {
        var customerProductName = $scope.phoneDet.name;
        var customerProductPrice = $scope.phoneDet.price;
        sessionStorage.setItem("customerProductName", customerProductName);
        sessionStorage.setItem("customerProductPrice", customerProductPrice);
        window.location = "#!/order";
    }
}]);
//------------------------End Phones------------------------------------------------------




//--------------------------Laptops--------------------------------------------------
//Create factory for laptop overview
productControllers.factory('laptopOverview', ['$firebaseArray', function ($firebaseArray) {
    //create a reference to the database where we will store our data
    var ref = new Firebase("https://ahihiweb.firebaseio.com/laptops/");

    //return it as a synchronized object
    return $firebaseArray(ref);

}]);

//Create controller for phone overview
productControllers.controller('laptopOverviewCtrl', ['$scope', 'laptopOverview',
    function ($scope, laptopOverview) {
        $scope.laptopOver  = laptopOverview;

        angular.forEach($scope.laptopOver, function (laptop) {
            laptop.price = parseFloat(laptop.price);
        });

        //Create function buy when click 'Đặt mua'
        $scope.buy = function (index) {
            var customerProductName = $scope.laptopOver.$getRecord(index).name;
            var customerProductPrice = $scope.laptopOver.$getRecord(index).price;
            sessionStorage.setItem("customerProductName", customerProductName);
            sessionStorage.setItem("customerProductPrice", customerProductPrice);
            window.location = "#!/order";
        }

    }]);

//Create factory for laptop detail
productControllers.factory('laptopDetail', ['$firebaseObject', '$routeParams', function ($firebaseObject, $routeParams) {
    //create a reference to the database where we will store our data
    var laptopId = $routeParams.laptopId;
    var ref = new  Firebase("https://ahihiweb.firebaseio.com/laptops/").child(laptopId);

    //return it as a synchronized object
    return $firebaseObject(ref);

}]);

//Create controller for laptop detail
productControllers.controller('laptopDetailCtrl', ['$scope', 'laptopDetail', function ($scope, laptopDetail) {
    $scope.laptopDet = laptopDetail;

    //Create function for saving editing phone
    $scope.saveInfo = function () {
        $scope.laptopDet.$save();
        alert("Lưu thành công");
        window.location = "#!/admin";
    };

    //Create function buy when click 'Đặt mua'
    $scope.buy = function () {
        var customerProductName = $scope.laptopDet.name;
        var customerProductPrice = $scope.laptopDet.price;
        sessionStorage.setItem("customerProductName", customerProductName);
        sessionStorage.setItem("customerProductPrice", customerProductPrice);
        window.location = "#!/order";
    }
}]);
//------------------------End Phones------------------------------------------------------





//-------------------------Product--------------------------------------------------------
//Create controller for product overview
productControllers.controller('productOverviewCtrl', ['$scope', 'phoneOverview', 'laptopOverview', 'authFactory',
    function ($scope, phoneOverview, laptopOverview, authFactory) {
        $scope.phoneOver  = phoneOverview;
        $scope.laptopOver = laptopOverview;

        //store a variable for searching at main page
        $scope.searchText = sessionStorage.getItem("comment");

        $scope.auth = authFactory.auth();

        // any time auth status updates, add the user data to scope
        $scope.auth.$onAuth(function(authData) {
            $scope.authData = authData;
        });
        
        //Create function add phone
        $scope.addPhone = function() {
            var n = $scope.phoneOver.length + 1;
            $scope.phoneOver.$add(
                {
                    id: n,
                    name: 'New phone ' + n,
                    imageUrl: 'http://',
                    imgDetail1:'http://',
                    imgDetail2:'http://',
                    imgDetail3:'http://',
                    imgLarge:'http://',
                    intro:'New Phone has just been added',
                    price:'VNĐ',
                    snipet: 'Slogan',
                    detail: {
                        cpu: 'CPU',
                        display: 'inch',
                        frontCam: 'PX',
                        behindCam: 'PX',
                        ram: 'GB',
                        os: 'OS',
                        flash: ' GB',
                        sim: 'SIM',
                        design: 'plastic',
                        connection: 'WIFI',
                        storage: 'GB',
                        specialFun: 'special',
                        battery: 'mAh'
                    },
                    review:{
                        intro: 'Heading title',
                        imgIntro: 'URL image for heading',
                        head1:'Paragraph 1 title',
                        head1con:'Paragraph 1 content',
                        head1img: 'URL image for paragraph 1',
                        head2:'Paragraph 2 title',
                        head2con:'Paragraph 2 content',
                        head2img: 'URL image for paragraph 2',
                        head3:'Paragraph 3 title',
                        head3con:'Paragraph 3 content',
                        head3img: 'URL image for paragraph 3',
                        head4:'Paragraph 4 title',
                        head4con:'Paragraph 4 content',
                        head4img: 'URL image for paragraph 4'
                    }
                });
        };

        //Create function add laptop
        $scope.addLaptop = function() {
            var n = $scope.laptopOver.length + 1;
            $scope.laptopOver.$add(
                {
                    id: n,
                    name: 'New laptop ' + n,
                    imageUrl: 'http://',
                    imgDetail1:'http://',
                    imgDetail2:'http://',
                    imgDetail3:'http://',
                    imgLarge:'http://',
                    intro:'New Laptop has just been added',
                    price:'VNĐ',
                    snipet: 'Slogan',
                    detail: {
                        cpu: 'CPU',
                        display: 'inch',
                        graphic: 'PX',
                        ram: 'GB',
                        os: 'OS',
                        connection: 'WIFI',
                        storage: 'GB',
                        battery: 'mAh'
                    },
                    review:{
                        intro: 'Heading title',
                        imgIntro: 'URL image for heading',
                        head1:'Paragraph 1 title',
                        head1con:'Paragraph 1 content',
                        head1img: 'URL image for paragraph 1',
                        head2:'Paragraph 2 title',
                        head2con:'Paragraph 2 content',
                        head2img: 'URL image for paragraph 2',
                        head3:'Paragraph 3 title',
                        head3con:'Paragraph 3 content',
                        head3img: 'URL image for paragraph 3',
                        head4:'Paragraph 4 title',
                        head4con:'Paragraph 4 content',
                        head4img: 'URL image for paragraph 4'
                    }
                });
        };

}]);
//-------------------------End Product----------------------------------------------