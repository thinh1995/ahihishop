/**
 * Created by XuanThinh on 07/06/2016.
 */
//----Customer
//Create factory for customer overview
productControllers.factory('customerOverview', ['$firebaseArray', function ($firebaseArray) {
    //create a reference to the database where we will store our data
    var ref = new Firebase("https://ahihiweb.firebaseio.com/customers/");

    //return it as a synchronized object
    return $firebaseArray(ref);

}]);

//Create controller for customer overview
productControllers.controller('customerOverviewCtrl', ['$scope', 'customerOverview',
    function ($scope, customerOverview) {
        $scope.customerOver  = customerOverview;
        
        $scope.cusProductName = sessionStorage.getItem("customerProductName");
        $scope.cusProductPrice = sessionStorage.getItem("customerProductPrice");

        //Create function for saving editing customer
        $scope.addInfo = function () {
            var n = $scope.customerOver.length + 1;
            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth()+1; //January is 0!
            var yyyy = today.getFullYear();

            if(dd<10) {
                dd='0'+dd
            }

            if(mm<10) {
                mm='0'+mm
            }

            today = dd+'/'+mm+'/'+yyyy;

            $scope.customerOver.$add({
                    product: sessionStorage.getItem("customerProductName"),
                    bill: sessionStorage.getItem("customerProductPrice"),
                    id: 'customer ' + n,
                    name: document.getElementById("customerName").value,
                    address: document.getElementById("customerAddress").value,
                    mobile: document.getElementById("customerMobile").value,
                    email: document.getElementById("customerEmail").value,
                    dayBirth: document.getElementById("customerDay").value,
                    monthBirth: document.getElementById("customerMonth").value,
                    yearBirth: document.getElementById("customerYear").value,
                    sex: document.getElementById("customerSex").value,
                    job: document.getElementById("customerJob").value,
                    company: document.getElementById("customerCompany").value,
                    interest: document.getElementById("customerInterest").value,
                    dateOrder: today,
                    status: 'ordered'
                }
            );
            alert("Đặt hàng thành công");
            window.location = "#!/";
        }

}]);

//Create factory for customer detail
productControllers.factory('customerDetail', ['$firebaseObject', '$routeParams', function ($firebaseObject, $routeParams) {
    //create a reference to the database where we will store our data
    var cusId = $routeParams.cusId;
    var ref = new  Firebase("https://ahihiweb.firebaseio.com/customers/").child(cusId);

    //return it as a synchronized object
    return $firebaseObject(ref);

}]);

//Create controller for customer detail
productControllers.controller('customerDetailCtrl', ['$scope', 'customerDetail', function ($scope, customerDetail) {
    $scope.customerDet = customerDetail;

    //create function save customer information
    $scope.saveInfo = function () {
        $scope.customerDet.$save();
        alert("Lưu thành công");
        window.location = "#!/orders";
    }
    
}]);