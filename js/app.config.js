/**
 * Created by XuanThinh on 29/05/2016.
 */
'use strict';

angular.
    module('myApp').
    config(['$locationProvider', '$routeProvider',
        function ($locationProvider, $routeProvider) {
            $routeProvider.
            when('/login', {
                templateUrl: 'login.html',
                controller: 'loginCtrl'
            }).
            when('/resetpass', {
                templateUrl: 'forgetpass.html',
                controller: 'loginCtrl'
            }).
            when('/logout', {
                templateUrl: 'login.html',
                controller: 'loginCtrl',
                resolve: {
                    "logout": ["authFactory", function (authFactory) {
                        authFactory.logout();
                    }]
                }
            }).
            when('/phones', {
                templateUrl: 'partials/phone-list.html',
                controller: 'phoneOverviewCtrl'
            }).
            when('/phones/:phoneId', {
                templateUrl: 'partials/phone-detail.html',
                controller: 'phoneDetailCtrl'
            }).
            when('/laptops', {
                templateUrl: 'partials/laptop-list.html',
                controller: 'laptopOverviewCtrl'
            }).
            when('/laptops/:laptopId', {
                templateUrl: 'partials/laptop-detail.html',
                controller: 'laptopDetailCtrl'
            }).
            when('/result', {
                templateUrl: 'result.html',
                controller: 'productOverviewCtrl'
            }).
            when('/', {
                templateUrl: 'index2.html',
                controller: 'productOverviewCtrl'
            }).
            when('/admin', {
                templateUrl: 'admin/admin.html',
                controller: 'productOverviewCtrl',
                resolve: {
                    "currentAuth" : ["authFactory", function (authFactory) {
                        var auth = authFactory.auth();
                        console.log(auth);
                        return auth.$requireAuth();
                    }]
                }
            }).
            when('/admin/editphone/:phoneId', {
                templateUrl: 'admin/phone-edit.html',
                controller: 'phoneDetailCtrl',
                resolve: {
                    "currentAuth" : ["authFactory", function (authFactory) {
                        var auth = authFactory.auth();
                        console.log(auth);
                        return auth.$requireAuth();
                    }]
                }
            }).
            when('/admin/editlaptop/:laptopId', {
                templateUrl: 'admin/laptop-edit.html',
                controller: 'laptopDetailCtrl',
                resolve: {
                    "currentAuth" : ["authFactory", function (authFactory) {
                        var auth = authFactory.auth();
                        console.log(auth);
                        return auth.$requireAuth();
                    }]
                }
            }).
            when('/order', {
                templateUrl: 'order.html',
                controller: 'customerOverviewCtrl'
            }).
            when('/orders', {
                templateUrl: 'admin/orders.html',
                controller: 'customerOverviewCtrl',
                resolve: {
                    "currentAuth" : ["authFactory", function (authFactory) {
                        var auth = authFactory.auth();
                        console.log(auth);
                        return auth.$requireAuth();
                    }]
                }
            }).
            when('/orders/:cusId', {
                templateUrl: 'admin/order-view.html',
                controller: 'customerDetailCtrl',
                resolve: {
                    "currentAuth" : ["authFactory", function (authFactory) {
                        var auth = authFactory.auth();
                        console.log(auth);
                        return auth.$requireAuth();
                    }]
                }
            }).
            when('/transfer', {
                templateUrl: 'transfer.html'
            }).
            when('/contact', {
                templateUrl: 'contact.html'
            }).
            otherwise({
                redirectTo : '/'
            });
            
            //$locationProvider.html5Mode(true);
            $locationProvider.hashPrefix("!");
}])

.run(['$rootScope','$location', function($rootScope, $location) {

    $rootScope.$on('$routeChangeError', function (event, next, previous, error) {
        console.log(error);
        if (error = "AUTH_REQUIRED") {

            console.log("Error in Auth");
            alert("Bạn không có quyền truya cập vào trang này!");
            $location.path("/login");
        }
    })
}])
;
