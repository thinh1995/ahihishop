/**
 * Created by XuanThinh on 30/05/2016.
 */
angular.module('myApp').factory('authFactory', ['$firebaseAuth', function($firebaseAuth) {
    var authFactory = {};
    var ref = new Firebase("https://ahihiweb.firebaseio.com");
    //Initialize FirebaseAuth
    var auth = $firebaseAuth(ref);
    authFactory.authUser = function(email, password) {
        return auth.$authWithPassword({
            email: email,
            password: password
        });
    }

    authFactory.createUser = function(email, password) {
        return auth.$createUser({
            email: email,
            password: password
        })};
    
    authFactory.resetUser = function (email) {
        return auth.$resetPassword({
             email: email
         })};

    authFactory.logout = function () {
        auth.$unauth();
    }

    authFactory.auth = function () {
        return auth;
    }

    return authFactory;
}]);

angular.module('myApp').run(function($rootScope, $templateCache) {
    $rootScope.$on('$viewContentLoaded', function() {
        $templateCache.removeAll();
    });
});