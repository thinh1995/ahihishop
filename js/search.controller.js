/**
 * Created by XuanThinh on 06/06/2016.
 */
/**
 * Created by XuanThinh on 05/06/2016.
 */
'use strict';

//Create function for search in banar
function searchCtrl() {
    var comment = document.getElementById("userInput").value;
    if (comment == "") {
        sessionStorage.setItem("comment", "");
        alert("Bạn chưa nhập từ khoá để tìm kiếm!!!");
        return false;
    }
    else {
        sessionStorage.setItem("comment", comment);
    }
    window.location = "#!/result";
    return false;
}