/**
 * Created by XuanThinh on 29/05/2016.
 */
'use strict';

angular.module('myApp', [
    'firebase',
    'ngRoute',
    'ngMessages',
    'productControllers'
]);
